/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.mondrian.api;

import java.net.URL;

/**
 * This service provides information about mondrian catalogs.
 */
// TODO - goingEclipse - move me away from here
public interface IMondrianService {

	/**
	 * This property is used in the properties of the OSGi service.
	 */
	static final String PROP__PACKAGE_NAME = "packageName";

	/**
	 * Returns the name of the cube package.
	 * 
	 * @return
	 */
	String getPackageName();

	/**
	 * Returns the URL for the mondrian catalog.
	 * 
	 * @return
	 */
	URL getMondrianCatalogURL();
	
	/**
	 * Gets the persistence unit of the first entity found in a cube model.
	 *
	 * @return the persistence unit
	 */
	String getPersistenceUnit();
	
	/**
	 * Gets the class loader of the first entity found in a cube model.
	 *
	 * @return the class loader
	 */
	ClassLoader getClassLoader();
}
